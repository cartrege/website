/* global React */
class Loading extends React.Component {
  render () {
    return (
      <div className='modal is-active'>
        <div className='modal-background' />
        <div className='modal-content has-text-centered animated rotateIn infinite'>
          <span className='icon is-large has-text-danger'>
            <ion-icon name='cog' class='is-size-1' />
          </span>
        </div>
      </div>
    )
  }
}
