/* global React */
class Menu extends React.Component {
  render () {
    return (
      <div className={this.props.className}>
        <p className='subtitle is-uppercase has-text-centered'>{this.props.title}</p>
        <div className='field'>
          <div className='control has-icons-right'>
            <input className='input is-small is-danger has-background-black' onChange={this.props.function} />
            <span className='icon is-small is-right'>
              <ion-icon name='search' />
            </span>
          </div>
        </div>
        <div className='menu'>
          <ul className='menu-list'>
            {this.props.children}
          </ul>
        </div>
      </div>
    )
  }
}
