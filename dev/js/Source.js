/* global React */
class Source extends React.Component {
  render () {
    return (
      <li>
        <ReactRouterDOM.NavLink strict sensitive activeClassName='is-active' to={this.props.to}>
          <p className='subtitle'>{this.props.source.source}</p>
        </ReactRouterDOM.NavLink>
      </li>
    )
  }
}
