/* global React, storage, getSourcesById */
class OnlinePage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'showLoading': storage.sources === null,
      'showError': false
    }
    this.renderTranslators = this.renderTranslators.bind(this)
    this.renderSeasons = this.renderSeasons.bind(this)
    this.renderEpisodes = this.renderEpisodes.bind(this)
    this.renderPlayer = this.renderPlayer.bind(this)
    this.index = {
      'source': null,
      'translator': null,
      'season': null,
      'episode': null
    }
  }

  componentDidMount () {
    if (storage.sources === null) {
      getSourcesById(this, this.props.match.params.query)
    }
  }

  renderTranslators (props) {
    this.index.source = parseInt(props.match.params.source_index)
    const translators = storage
      .sources[this.index.source]
      .translators
    return <Translators pathname={props.match.url} translators={translators} />
  }

  renderSeasons (props) {
    this.index.translator = parseInt(props.match.params.translator_index)
    const seasons = storage
      .sources[this.index.source]
      .translators[this.index.translator]
      .seasons
    return <Seasons pathname={props.match.url} seasons={seasons} />
  }

  renderEpisodes (props) {
    this.index.season = parseInt(props.match.params.season_index)
    const episodes = storage
      .sources[this.index.source]
      .translators[this.index.translator]
      .seasons[this.index.season]
      .episodes
    return <Episodes pathname={props.match.url} episodes={episodes} />
  }

  renderPlayer (props) {
    let query
    if (storage.hasEpisodes === true) {
      this.index.episode = parseInt(props.match.params.episode_index)
      query = storage
        .sources[this.index.source]
        .translators[this.index.translator]
        .seasons[this.index.season]
        .episodes[this.index.episode]
        .query
    } else {
      this.index.translator = parseInt(props.match.params.translator_index)
      query = storage
        .sources[this.index.source]
        .translators[this.index.translator]
        .query
    }
    const translator = storage
      .sources[this.index.source]
      .translators[this.index.translator]
    return <Player hostname={translator.hostname} pathname={translator.pathname} query={query} />
  }

  render () {
    if (this.state.showLoading === true) {
      return <Loading />
    }
    if (this.state.showError === true) {
      return <Error error={storage.error} />
    }
    let additionalElements = ''
    let additionalPath = ''
    if (storage.hasEpisodes === true) {
      additionalElements = (
        <React.Fragment>
          <ReactRouterDOM.Route strict sensitive path='/query/:query/online/:source_index/:translator_index/' render={this.renderSeasons} />
          <ReactRouterDOM.Route strict sensitive path='/query/:query/online/:source_index/:translator_index/:season_index/' render={this.renderEpisodes} />
        </React.Fragment>
      )
      additionalPath = ':season_index/:episode_index/'
    }
    return (
      <React.Fragment>
        <div className='section'>
          <div className='columns is-mobile'>
            <Sources pathname={this.props.match.url} sources={storage.sources} />
            <ReactRouterDOM.Route strict sensitive path='/query/:query/online/:source_index/' render={this.renderTranslators} />
            {additionalElements}
          </div>
        </div>
        <ReactRouterDOM.Route strict sensitive path={`/query/:query/online/:source_index/:translator_index/${additionalPath}`} render={this.renderPlayer} />
      </React.Fragment>
    )
  }
}
