/* global React */
class Season extends React.Component {
  render () {
    return (
      <li>
        <ReactRouterDOM.NavLink strict sensitive activeClassName='is-active' to={this.props.to}>
          <p className='subtitle'>{this.props.season.season} сезон</p>
          <div className='tags'>
            <span className='tag is-dark'>Серий: {this.props.season.episodes.length}</span>
          </div>
        </ReactRouterDOM.NavLink>
      </li>
    )
  }
}
