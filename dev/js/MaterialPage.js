/* global React, storage, getMaterialById */
class MaterialPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'showLoading': storage.material === null,
      'showError': false
    }
  }

  componentDidMount () {
    if (storage.material === null) {
      getMaterialById(this, this.props.match.params.query)
    }
  }

  renderValues (title, data) {
    if (data === null) {
      return ''
    }
    return (
      <React.Fragment>
        <div className='column is-2'>
          <p className='has-text-weight-bold'>{title}</p>
        </div>
        <div className='column is-10'>
          <div className='tags'>
            {data.map(value => <span className='tag is-light'>{value}</span>)}
          </div>
        </div>
      </React.Fragment>
    )
  }

  render () {
    if (this.state.showLoading === true) {
      return <Loading />
    }
    if (this.state.showError === true) {
      return <Error error={storage.error} />
    }
    return (
      <div className='notification is-dark animated jackInTheBox'>
        <div className='columns is-centered is-mobile is-multiline'>
          <div className='column is-12-mobile is-9-tablet is-9-desktop is-10-widescreen is-10-fullhd'>
            <p className='title'>
              <span className='is-uppercase'>
                {storage.material.title}
              </span>
              <span>&nbsp;</span>
              <sup>
                <span className='tag is-danger'>
                  {storage.material.year}
                </span>
              </sup>
            </p>
            <p className='subtitle'>
              {storage.material.tagline}
            </p>
            <div className='buttons'>
              <ReactRouterDOM.NavLink strict sensitive className='button is-danger is-rounded' to={`${this.props.match.url}online/`}>
                <span className='icon is-small'>
                  <ion-icon name='play' />
                </span>
                <span>Смотреть онлайн</span>
              </ReactRouterDOM.NavLink>
              <ReactRouterDOM.NavLink strict sensitive className='button is-white is-rounded is-outlined' to={`${this.props.match.url}trailer/`}>
                <span className='icon is-small'>
                  <ion-icon name='play' />
                </span>
                <span>Смотреть трейлер</span>
              </ReactRouterDOM.NavLink>
              <ReactRouterDOM.NavLink strict sensitive className='button is-white is-rounded is-outlined is-static' to={`${this.props.match.url}torrents/`}>
                <span className='icon is-small'>
                  <ion-icon name='download' />
                </span>
                <span>Скачать торрент</span>
              </ReactRouterDOM.NavLink>
            </div>
            <div className='content'>
              <p>
                {storage.material.description}
              </p>
            </div>
            <div className='columns is-centered is-mobile is-multiline'>
              {this.renderValues('Жанры', storage.material.genres)}
              {this.renderValues('Актёры', storage.material.actors)}
              {this.renderValues('Режиссёры', storage.material.directors)}
              {this.renderValues('Студии', storage.material.studios)}
            </div>
          </div>
          <div className='column is-hidden-mobile is-3-tablet is-3-desktop is-2-widescreen is-2-fullhd'>
            <figure className='image is-2by3'>
              <img src={`https://st.kp.yandex.net/images/film_iphone/iphone360_${this.props.match.params.query}.jpg`} />
            </figure>
            <br />
            <div className='level is-mobile'>
              <div className='level-item has-text-centered'>
                <div>
                  <p className='is-size-4 has-text-danger'>
                    {storage.material.imdb.rating === null ? 'Н/Д' : storage.material.imdb.rating.toFixed(2)}
                  </p>
                  <p className='heading'>IMDb</p>
                </div>
              </div>
              <div className='level-item has-text-centered'>
                <div>
                  <p className='is-size-4 has-text-danger'>
                    {storage.material.kinopoisk.rating === null ? 'Н/Д' : storage.material.kinopoisk.rating.toFixed(2)}
                  </p>
                  <p className='heading'>КиноПоиск</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
