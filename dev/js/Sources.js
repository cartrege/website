/* global React, storage */
class Sources extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'query': ''
    }
    this.onInputChange = this.onInputChange.bind(this)
  }

  onInputChange (event) {
    this.setState({
      'query': event.target.value.trim().toLowerCase()
    })
  }

  render () {
    return (
      <Menu className={`column ${storage.hasEpisodes === true ? 'is-3' : 'is-6'} animated slideInDown`} title='Источники' function={this.onInputChange}>
        {
          this
            .props
            .sources
            .map((source, sourceIndex) => source
              .source
              .startsWith(this.state.query) === true && <Source to={`${this.props.pathname}${sourceIndex}/`} source={source} />)
        }
      </Menu>
    )
  }
}
