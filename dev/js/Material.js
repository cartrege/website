/* global React */
class Material extends React.Component {
  render () {
    return (
      <div key={this.props.material.id} className='column is-6-mobile is-3-tablet is-3-desktop is-2-widescreen is-2-fullhd'>
        <div className='card'>
          <ReactRouterDOM.Link target='_blank' to={`/query/${this.props.material.id}/`} className='card-image has-hover-toggle'>
            <figure className='image is-2by3'>
              <img src={`https://st.kp.yandex.net/images/film_iphone/iphone360_${this.props.material.id}.jpg`} />
            </figure>
            <div className='is-overlay is-toggled is-flex is-flex-centered modal-background'>
              <span className='icon is-large has-text-danger'>
                <ion-icon name='play' class='is-size-1' />
              </span>
            </div>
            <div className='is-overlay is-toggled is-flex is-flex-bottom'>
              <div className='level is-mobile'>
                <div className='level-item has-text-centered'>
                  <div>
                    <p className='heading has-text-danger'>IMDb</p>
                    <p className='title is-6'>{this.props.material.imdb.rating === null ? 'Н/Д' : this.props.material.imdb.rating.toFixed(2)}</p>
                  </div>
                </div>
                <div className='level-item has-text-centered'>
                  <div>
                    <p className='heading has-text-danger'>КиноПоиск</p>
                    <p className='title is-6'>{this.props.material.kinopoisk.rating === null ? 'Н/Д' : this.props.material.kinopoisk.rating.toFixed(2)}</p>
                  </div>
                </div>
              </div>
              <br />
            </div>
            <div className='is-overlay'>
              <span className='tag is-radiusless is-dark is-absolute'>{this.props.material.year}</span>
            </div>
          </ReactRouterDOM.Link>
        </div>
        <p className='is-size-7 has-text-grey-light has-text-centered'>{this.props.material.title}</p>
      </div>
    )
  }
}
