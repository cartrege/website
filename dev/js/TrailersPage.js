/* global React, storage, getMaterialById, getYouTubeTrailersByName */
class TrailersPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'showLoading': storage.trailers === null,
      'showError': false
    }
  }

  componentDidMount () {
    if (storage.trailers === null) {
      if (storage.material === null) {
        getMaterialById(this, this.props.match.params.query, true)
          .then(() => getYouTubeTrailersByName(this, storage.material.title))
      } else {
        getYouTubeTrailersByName(this, storage.material.title)
      }
    }
  }

  renderPlayer (props) {
    const trailer = storage.trailers[props.match.params.trailer_index]
    return <Player hostname={trailer.hostname} pathname={trailer.pathname} query={trailer.query} />
  }

  render () {
    if (this.state.showLoading === true) {
      return <Loading />
    }
    if (this.state.showError === true) {
      return <Error error={storage.error} />
    }
    return (
      <React.Fragment>
        <div className='menu'>
          <ul className='menu-list'>
            {
              storage.trailers.map((trailer, trailerIndex) => (
                <li>
                  <ReactRouterDOM.NavLink strict sensitive exact activeClassName='is-active' to={`${this.props.match.url}${trailerIndex}/`} >
                    <div className='columns'>
                      <div className='column is-2'>
                        <figure className='image is-4by3'>
                          <img src={`https://i.ytimg.com/vi/${trailer.id}/hqdefault.jpg`} />
                        </figure>
                      </div>
                      <div className='column is-10'>
                        <p className='is-size-6'>{trailer.title}</p>
                        <br />
                        <p className='is-size-6'>
                          <span className='tag is-dark'>{trailer.channel}</span>
                        </p>
                      </div>
                    </div>
                  </ReactRouterDOM.NavLink>
                </li>
              ))
            }
          </ul>
        </div>
        <ReactRouterDOM.Route strict sensitive exact path='/query/:query/trailer/:trailer_index/' render={this.renderPlayer} />
      </React.Fragment>
    )
  }
}
