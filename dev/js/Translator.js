/* global React, storage */
class Translator extends React.Component {
  render () {
    const tags = []
    if (storage.hasEpisodes === true) {
      tags.push(`Сезонов: ${this.props.translator.seasons.length}`)
    } else {
      switch (this.props.translator.has_ads) {
        case true:
          tags.push('Есть реклама')
          break
        case false:
          tags.push('Без рекламы')
          break
      }
      switch (this.props.translator.is_camrip) {
        case true:
          tags.push('Экранка')
          break
      }
      switch (this.props.translator.is_extended) {
        case true:
          tags.push('Расширенная версия')
          break
      }
      switch (this.props.translator.quality) {
        case null:
          break
        default:
          tags.push(this.props.translator.quality)
      }
    }
    return (
      <li>
        <ReactRouterDOM.NavLink strict sensitive activeClassName='is-active' to={this.props.to}>
          <p className='subtitle'>{this.props.translator.translator}</p>
          <div className='tags'>
            {
              tags.map(tag => <span className='tag is-dark'>{tag}</span>)
            }
          </div>
        </ReactRouterDOM.NavLink>
      </li>
    )
  }
}
